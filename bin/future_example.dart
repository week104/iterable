Future<String> createOrderMessage() async {
  var order = await fetchUserOder();
  return 'Your order is: $order'; 
}

Future<String> fetchUserOder() =>Future.delayed(
  const Duration(seconds: 2),
  () => 'Large Latte',
);
  

void main() async{
  print('Fetching user order...');
  print(await createOrderMessage());
  print('End of create order message');
}